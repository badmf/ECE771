\documentclass[12pt,letter]{article}
\usepackage{include}
\usepackage{float}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{placeins}
\usepackage[nottoc,numbib]{tocbibind}
\usepackage{mathtools}
\usepackage{hyperref}

% layout
\setlength{\hoffset}{-1in}
\setlength{\voffset}{-1in}
\setlength{\topmargin}{2cm}
\setlength{\headsep}{1cm}
\setlength{\headheight}{16pt}
\setlength{\marginparwidth}{0mm}
\setlength{\marginparsep}{0cm}
\setlength{\oddsidemargin}{18mm}
\setlength{\evensidemargin}{18mm}
\setlength{\textwidth}{18cm}
\setlength{\textheight}{22cm}
\setlength{\parindent}{0pt}
\pagestyle{fancy}

% heading, footing
\lhead{ECE771 - term project: Sound Source Localization}
\chead{}
\rhead{Kampelmuehler}
\lfoot{}
\cfoot{\thepage}
\rfoot{}

\usepackage[square,numbers]{natbib}
\bibliographystyle{abbrvnat} 

\begin{document}


% titlepage
\thispagestyle{empty}
\vspace*{-1cm}
\hfill
\begin{minipage}{4cm}
\includegraphics[width=40mm]{./img/MCMlogo}
\end{minipage}\\
\vspace*{2.2cm}
\begin{center}
  \hrulefill
  \textsc{\large Project Report}
  \hspace{-2mm} \hrulefill \\[5.5mm]
  \parbox{\textwidth}{\centering\scshape\huge\linespread{0.9}\selectfont 
	ECE771 - term project:\\ Sound Source Localization}
  \\[4mm] \hrulefill \\[2mm]
  %{\centering\parbox{0.9\textwidth}{\centering\Large Subtitle}} \\[2.0cm] \par
  written at\\ Electrical and Computer Engineering Grad Department
 \\ 
McMaster University
  \\[1cm]\par by \\  Kampelmuehler, Moritz (400108331)
  \\[1.75cm]\par
  \vfill{\flushright \today}\\
\end{center}

\clearpage
%\tableofcontents

\clearpage


\section{Introduction}

The aim of this project should be to estimate the position of a sound source using the raw data recorded by preferably just two microphones (i.e. binaural localization).\\

A basic experimental setup is depicted in Figure \ref{fig:basic}. Herein the sound source is located off-center --- the center being $C$. This will introduce a time difference ($t_1-t_2$) between the two receptors (microphones). Under certain assumptions it is thus possible to estimate the position of a speaker. Here restrictions are necessary since the position of the speaker using just two microphones results in a hyperboloid surface of possible solutions \cite{omologo05}.\\

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.70\textwidth]{img/basic_sketch.png}
\caption{Basic setup}
\label{fig:basic}
\end{center}
\end{figure}

Speaker localization using two or more microphones is of interest for different applications. One of them being noise reduction by applying beamforming techniques to focus the receptive field of the sensor towards the estimated location. A hands-free approach is essential for reducing speaker impairment \citep{yamada96}. The estimated speaker location might also be used to steer a camera towards a speaker for automated lecture or talk recordings \citep{chakrabarty14}.\\

Binaural localization is of particular interest for robotics, where options for sensors are limited, as well as in rehabilitation engineering where results of such research might be incorporated in to Cochlear Implants (CIs). Both of those cases are especially challenging because of arbitrary movement of the receiver and source relative to one another \citep{portello11}.

\section{Model description}

For acquiring the position of a sound source using two or more microphone the immediate measurement quantity is in general \textit{time difference of arrival --- TDOA} . Depending on where the source is located, the sound waves take specific amounts of time to travel to either sensor. This time delay is most commonly measured by using the \textit{generalized cross correlation --- GCC} \citep{omologo94}:

\begin{equation}
R_{12}(\tau) = \frac{1}{2\pi} \int_{-\infty}^{\infty}
\frac{X_1(e^{j\omega\tau})X_2^*(e^{j\omega\tau})}
{|X_1(e^{j\omega\tau})X_2^*(e^{j\omega\tau})|}
e^{j\omega\tau} d\omega.
\end{equation}

The TDOA is estimated as the lag of the maximum of the cross correlation function.

After having acquired the TDOA it can under certain assumptions be converted into a source location. For sake of linearity and uniqueness of the solution in this project the following assumptions are made:

\begin{itemize}
\item The height of the speaker is ignored
\item The speaker's movement is restricted to a straight line
\item Said line is parallel to the microphone axis
\end{itemize}

Under those assumptions, the source position along an axis $x$ can be derived from the TDOA as follows:

\begin{equation}
x = \frac{d_M}{2}
\frac{c}{\sqrt{y_0^2 + d_M^2}-y_0}
\Delta t,
\end{equation}

where

\begin{align*}
c &= \text{speed of sound} \\
d_M &= \text{distance between microphones} \\
y_0 &= \text{distance from microphones to speaker axis} \\
\Delta t &= \text{TDOA}.
\end{align*}

This way the position measurement can directly be acquired. Since the measured position is subject to noise a Kalman Filter should be used to provide a more accurate estimate of the source state.

A common choice to model the dynamics of the source's movement is a \textit{random walk} model \citep{chakrabarty14}, which is similar to a \textit{white noise acceleration model --- WNA}. According to those models the state's evolution is given by

\begin{equation}
\mathbf{x_k = x_{k-1} + q_k},
\end{equation}

where $q_k = \mathcal{N}(0,q)$ is the random control sequence.

The discrete time WNA model used to model the speaker's movement is defined as follows

\begin{equation}
\mathbf{x} = \begin{bmatrix}
         x \\
         \dot{x}
        \end{bmatrix}
, \hspace{6pt}
\mathbf{F} = \begin{bmatrix}
         1 & T \\
         0 & 1
        \end{bmatrix}
, \hspace{6pt}
\mathbf{B} = \begin{bmatrix}
         \frac{T^2}{2} \\
         T
        \end{bmatrix}
, \hspace{6pt}
\mathbf{Q} = \begin{bmatrix}
         \frac{T^4}{4} & \frac{T^3}{2} \\
         \frac{T^3}{2} & T^2
        \end{bmatrix} \sigma_x^2
, \hspace{6pt}
\mathbf{R} = \mathbf{I} \sigma_n^2
\end{equation}

\section{Experiments}

For the movement of a sound source two states are assumed, namely constant velocity movement and standstill. Thus trajectories are being simulated which alternate between those two states. The alternations can be considered as maneuvers. It is assumed that the measurement is accurate within $\sigma_n = 0.1ms^{-2}$, which lies within reason for the method being used, but might not be universally applicable. This is due to the fact, that the measurement noise depends on the specific system being used and the acoustic characteristics of the environment the system is used in.\\

The position of the sound source is first tracked by a Kalman Filter according to the model described earlier.
In order to account for the maneuvers some process noise has to be assumed as well, which for the specific implementation was set to $\sigma_x = 2.81*\cdot10^{-3}ms^{-2}$. All filters were initialized according to a single-point initialization as proposed by \cite{mallick08}.\\

All the code was implemented in python, partially using \texttt{filterpy} \cite{labbe14}. Therefore a framework was created to provide a basic structure for filter tuning applications. The code is written in a way, that all parameters can be easily modified and such that plots and quality measures can be generated.

An example simulated trajectory of a moving sound source is shown in Figure \ref{fig:traj_lng}, where the blue chart is the actual position, green is the measurements and red is the track generated by the filter.\\

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.70\textwidth]{img/trajectory_long.png}
\caption{Simulated trajectory (long): True, Measurements and Estimate. IMM estimator.}
\label{fig:traj_lng}
\end{center}
\end{figure}

To achieve an even better estimate an IMM estimator \cite{blom88, genovese01} was used, using three different models of motion: a low-noise CV model ($\sigma_x\cdot10^{-3}$), a WNA model and a low-noise zero velocity model ($\sigma_x\cdot10^{-4}$). It has shown that using eg. two identical models where one is incorporating a high amount of process noise and a noiseless model is not beneficial in the IMM algorithm. If the difference is too large, the model which should respond to maneuvers is neglected in the transition probabilities.

This way the filter should be responsive to maneuvers of the tracked sound source while still providing a somewhat smooth estimate in maneuver free periods. For the mode transition matrix, the following values have been found to yield the best results:

\begin{equation}
\mathbf{M} = \begin{bmatrix}
         0.92 & 0.02 & 0.06 \\
         0.025 & 0.95 & 0.025 \\
         0.075 & 0.025 & 0.90 \\
        \end{bmatrix} 
\end{equation}

In order to compare the two estimates of the single KF approach and the IMM estimator several different trajectories were simulated in which the number of maneuvers and the length of the periods of stillness and movement were varied. The trajectory in Figure \ref{fig:traj_lng} is an example for a trajectory with many maneuvers and a long duration cycle (100 samples). The IMM estimator is fairly quick at responding to maneuvers (i.e. with small lag) while it also allows for a smoother estimate in the maneuver free sections of the trajectory, where it outperforms the single Kalman Filter.\\

For testing of consistency a \textit{chi-square test} based on \textit{normalized estimation error squared - NEES} \cite{li01} has also been implemented. A scheme for testing estimators in Monte Carlo runs is suggested in \citep{bar04}. However it turned out that this test is not feasible for maneuvering targets, since the chi-square bounds are violated to frequently. It might be possible to alter the method in a way to be valid for maneuvering targets and multiple models as well. This might be considered in future research.\\

It was thus necessary to come up with another measure of estimate quality. To compare the performance of the two algorithms against one another, the MSE was used. Using the knowledge of the true trajectory and simulated for both trajectories over 100 Monte Carlo runs the factor $\frac{\text{MSE}_{measurement}}{\text{MSE}_{estimator}}$ was investigated to provide a measure for the improvement in MSE provided by the estimation algorithms. The higher this factor, the better the improvement the estimator yields. Table \ref{tab:MSE} shows some of the notable results achieved.

\begin{table}[htbp]
\begin{center}
\begin{tabular}
{c || c | c | c}

$\frac{\text{MSE}_{measurement}}{\text{MSE}_{estimator}}$ & Single KF & IMM Estimator & Improvement over single KF\\[0.5em]
\hline\hline
short trajectory/short cycle  & 4.19 & 4.56 & +9\%\\
long trajectory/long cycle  & 5.63 & 6.95 & +23\%

\end{tabular}
\caption{\label{tab:MSE}MSE factor: Single KF vs. IMM Estimator}
\end{center}
\end{table}

As expected the IMM estimator is able to outperform a single Kalman Filter in high and low maneuverability cases. Note however that the true advantage of the IMM estimator lies in providing a more solid estimate in the case of tracking targets that are preferably remaining in a specific order of motion for longer periods of time. Since it provides an overall increase in quality of estimate it should be the preferred algorithm if no computational restrictions apply.

\section{Conclusion}

We suggested the IMM algorithm for sound source localization and were able to show that it is indeed capable of outperforming a single Kalman Filter in providing a smoother estimate of the sound source position. However, it has to be said, that both filters are able to provide a notable amount of noise reduction. For a three-model approach as used in the experiments of this project only a marginal increase in computational complexity is to be expected compared to a single Kalman Filter. The major downfall of the IMM algorithm is the increased complexity concerning the tuning of the filter. It turned out that parameters are highly interdependent, so all of the parameters have to be tuned with care. In this case this was done by hand, which certainly is not optimal. An interesting opportunity of further research might thus be to come up with a way of filter tuning for IMM algorithms based on numerical optimization or even machine learning techniques. Another further step would be to provide a practical implementation for verification of the simulations and further optimization of the models.\\


From here several further steps could be taken to elaborate on the observations made. In general, depending on the application, it might be favorable to extend the algorithm to deal with inputs from multiple microphones, to further reduce the influence of noise \cite{chen03}. This way by incorporating $M$ microphones the noise power can be reduces by a factor of $10 log(M)$ \cite{drews96}.\\

Furthermore the restrictions of this project could be lifted to generalize to tracking in three dimensional space. Again, this would require the usage of microphone arrays in specific constellation. An option for this would be to arrange a set of microphones in a square. This would allow to retrieve spacial information by intelligently choosing microphone pairs across the array \cite{drews96}.\\

Since however the problem of tracking in three dimensions becomes inherently non-linear the application of non-linear filters is mandatory. Approaches for using a single Extended Kalman Filter for sound source localization have been suggested \citep{klee06}, none of them however assume multiple models. Considering the characteristics of the movement of a human speaker, who in general might not move around frequently, an IMM estimator might indeed be able to increase localization accuracy.\\

A last suggestion to would be to incorporate \textit{interaural level differences --- ILD} into the binaural approach, by acoustically separating the microphones. Using the transfer function of the separation of the sensor the ILD information can be used as additional information for decreasing noise in the system and thus increasing localization accuracy.\\[2em]

All code can be found on a git repository: \url{https://goo.gl/Dpef7I}. The files used for the final simulation are to be found in \texttt{source/IMM\_nees\_final/}.

\clearpage
\small{\bibliography{references}}

\end{document}

%\begin{figure}[htbp]
%\begin{center}
%\includegraphics[width=\textwidth]{img/bla.png}
%\caption{bla}
%\label{fig:}
%\end{center}
%\end{figure}


%\begin{table}[htbp]
%\begin{center}
%\begin{tabular}
%{c | c | c | c}

%titleline & & & \\
%\hline
%content & & & \\
%content & & &

%\end{tabular}
%\caption{\label{tab:}caption}
%\end{center}
%\end{table}

%\begin{lstlisting}[caption={},captionpos=b,frame=single]
%...
%\end{lstlisting}
