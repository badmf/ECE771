#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)
import numpy as np
import matplotlib.pyplot as plt

def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))

# load data
npzfile = np.load('data.npz')
x_true = npzfile['x']
zs = npzfile['z']
T = npzfile['T']
t = npzfile['t']
A = npzfile['A']
sigma_n = npzfile['sigma_n']
sigma_x = npzfile['sigma_x']
a = npzfile['a']
B = npzfile['B']

# data length
n = zs.shape[0]

dim_x = 2
dim_z = 1

# define constant parameters
x = np.array([[0], [0]], dtype=float) # initial state (location and velocity)
u = np.array([a], dtype=float) # acceleration
F = A # state transition matrix
H = np.array([[1,0]], dtype=float) # Measurement function
G = B  # control function
R = np.eye(dim_z) * ((sigma_n)**2) # measurement noise
Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x)**2) # process noise
P = np.eye(dim_x, dtype=float) * 1e3 # covariance matrix

pos = [None] * n
vel = [None] * n
cov = [None] * n
inncov = [None] * n
inn = [None] * n
NIS = [None] * n
Gains = [None] * n

# Kalman Filter
	
for i in range(n):
	pos[i] = x[0,0]
	vel[i] = x[1,0]
	cov[i] = P

	# predict
	x_est = np.dot(F,x) + np.dot(G,u)
	P_est = dot3(F,P,F.T) + Q
	
	# update
	v = zs[i] - np.dot(H,x_est)
	S = dot3(H,P_est,H.T) + R
	
	W = dot3(P_est,H.T,np.linalg.inv(S))
	x = x_est + np.dot(W,v)
	#~ P = np.dot((np.eye(dim_x) - np.dot(W,H)),P_est)
	# Joseph form covariance update
	I_WH = np.eye(dim_x) - np.dot(W,H)
	P = dot3(I_WH,P_est,I_WH.T)+dot3(W,R,W.T)

	inncov[i] = S
	inn[i] = v
	NIS[i] = dot3(v.T,np.linalg.inv(S),v)
	Gains[i] = W

print('mse: ' + str(((x_true - pos)**2).mean()))

p0, = plt.plot(t, x_true)
p1, = plt.plot(t,zs, linestyle='dashed')
p2, = plt.plot(t,pos)
plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=2)
plt.ylim((max(np.min(pos),np.min(zs))-0.5,max(np.max(pos),np.max(zs))+0.5))
plt.ylabel('x [m]')
plt.xlabel('t [s]')
plt.title('Variable Velocity Kalman Filter')
plt.show()

#~ ps = []
#~ for p in cov:
	#~ ps.append(p[(0,0)[0],(0,0)[1]])
#~ plt.plot(ps)
#~ plt.title('Changing Velocity Kalman Filter - Covariance')
#~ plt.show()
