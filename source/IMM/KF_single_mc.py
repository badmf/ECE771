#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)
import numpy as np
import matplotlib.pyplot as plt
import os

def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))
	
n_runs = 100
mse_zs = []
mses = []
mseratios = []

for i in range(n_runs):
	
	os.system("./generate_data.py")
	
	# load data
	npzfile = np.load('data.npz')
	x_true = npzfile['x']
	zs = npzfile['z']
	T = npzfile['T']
	t = npzfile['t']
	A = npzfile['A']
	sigma_n = npzfile['sigma_n']
	sigma_x = npzfile['sigma_x']
	a = npzfile['a']
	B = npzfile['B']
	
	# data length
	n = zs.shape[0]
	
	dim_x = 2
	dim_z = 1
	
	# "two point differencing init"
	v0 = 0
	p0 = zs[0]
	
	# define constant parameters
	x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
	F = A # state transition matrix
	H = np.array([[1,0]], dtype=float) # Measurement function
	R = np.eye(dim_z) * ((sigma_n)**2) # measurement noise
	Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*.71)**2) # process noise
	P = np.eye(dim_x, dtype=float) * (sigma_n)**2 # covariance matrix
	
	pos = [None] * n
	vel = [None] * n
	cov = [None] * n
	inncov = [None] * n
	inn = [None] * n
	NIS = [None] * n
	Gains = [None] * n
	
	# Kalman Filter
	
	for i in range(n):
		
		pos[i] = x[0,0]
		vel[i] = x[1,0]
		cov[i] = P
			
		# predict
		x_est = np.dot(F,x)
		P_est = dot3(F,P,F.T) + Q
		
		# update
		v = zs[i] - np.dot(H,x_est)
		S = dot3(H,P_est,H.T) + R
		
		W = dot3(P_est,H.T,np.linalg.inv(S))
		x = x_est + np.dot(W,v)
		# Joseph form covariance update
		I_WH = np.eye(dim_x) - np.dot(W,H)
		P = dot3(I_WH,P_est,I_WH.T)+dot3(W,R,W.T)
	
		inncov[i] = S[0]
		inn[i] = v[0]
		NIS[i] = dot3(v.T,np.linalg.inv(S),v)[0]
		Gains[i] = W[0]
	
	mse = (((x_true - pos)**2).mean())
	mse_z = (((x_true - zs)**2).mean())
	mses.append(((x_true - pos)**2).mean())
	mse_zs.append(((x_true - zs)**2).mean())
	mseratios.append(mse_z/mse)
	

temp = np.asarray(mseratios)
print(temp.mean())
