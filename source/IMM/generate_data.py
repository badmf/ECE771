#!/usr/bin/env python

# used to generate a data vector for estimation
# vector z contains noisy position measurements
# Moritz Kampelmuehler
# 12/16

import numpy as np
import matplotlib.pyplot as plt


# constants
dM = float(0.3) #[m]
y0 = float(0.5) #[m]
c = float(343) #[m/s]
T = float(1e0) #[s]
n = 50 # signal length [samples]
A = np.array([[1,T], [0,1]], dtype=float) # transition matrix
B = np.array([[T**2/2], [T]], dtype=float) # control matrix
a = float(0) # constant acceleration
v0 = float(1) # constant velocity

# noise parameters
mu_n = 0
sigma_n = float(1e-1)
mu_x = 0
sigma_x = float(1e-3)

# trajectory boundaries
x0 = -0.5
x1 = 0.5



x = np.hstack((np.linspace(x0,x1,n),np.ones(n) * x1))
x = np.hstack((x, np.linspace(x1,x0,n)))
x = np.hstack((x, np.ones(2*n) * x0))

# repeat for 'long' trajectory
#~ x = np.tile(x,5)
	
t = np.arange(np.shape(x)[0]) * T
z = x + np.random.normal(mu_n, sigma_n, np.shape(x)[0])

# store data
np.savez('data.npz', x=x, z=z, T=T, A=A, B=B, a=a, sigma_n=sigma_n, sigma_x=sigma_x, t=t, v0=v0)


# plot data
plt.plot(np.arange(x.shape[0]), x, 'ro')
plt.plot(np.arange(z.shape[0]), z, 'bo')
#~ plt.show()
