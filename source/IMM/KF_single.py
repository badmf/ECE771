#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (WNA)
# Moritz Kampelmuehler
# 12/16

import numpy as np
import matplotlib.pyplot as plt

def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))

# load data
npzfile = np.load('data.npz')
x_true = npzfile['x']
zs = npzfile['z']
T = npzfile['T']
t = npzfile['t']
A = npzfile['A']
sigma_n = npzfile['sigma_n']
sigma_x = npzfile['sigma_x']
a = npzfile['a']
B = npzfile['B']

# data length
n = zs.shape[0]

dim_x = 2
dim_z = 1

# "two point differencing init"
v0 = 0
p0 = zs[0]

# define constant parameters
x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
F = A # state transition matrix
H = np.array([[1,0]], dtype=float) # Measurement function
R = np.eye(dim_z) * ((sigma_n)**2) # measurement noise
Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*.71)**2) # process noise
P = np.eye(dim_x, dtype=float) * (sigma_n)**2 # covariance matrix

pos = [None] * n
vel = [None] * n
cov = [None] * n
inncov = [None] * n
inn = [None] * n
NIS = [None] * n
Gains = [None] * n

# Kalman Filter

for i in range(n):
	
	pos[i] = x[0,0]
	vel[i] = x[1,0]
	cov[i] = P
		
	# predict
	x_est = np.dot(F,x)
	P_est = dot3(F,P,F.T) + Q
	
	# update
	v = zs[i] - np.dot(H,x_est)
	S = dot3(H,P_est,H.T) + R
	
	W = dot3(P_est,H.T,np.linalg.inv(S))
	x = x_est + np.dot(W,v)
	# Joseph form covariance update
	I_WH = np.eye(dim_x) - np.dot(W,H)
	P = dot3(I_WH,P_est,I_WH.T)+dot3(W,R,W.T)

	inncov[i] = S[0]
	inn[i] = v[0]
	NIS[i] = dot3(v.T,np.linalg.inv(S),v)[0]
	Gains[i] = W[0]

mse = ((x_true - pos)**2).mean()
mse_z = ((x_true - zs)**2).mean()

print('estimation MSE: ' + str(mse))
print('input MSE: ' + str(mse_z))
print('input MSE/estimation MSE: ' + str(mse_z/mse))

# plot position, true, estimate + measurement
pplt = plt.figure(1)
p0, = plt.plot(t, x_true)
p1, = plt.plot(t,zs, linestyle='dashed')
p2, = plt.plot(t,pos)
plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=1)
plt.ylim((max(np.min(pos),np.min(zs))-0.5,max(np.max(pos),np.max(zs))+0.5))
plt.ylabel('x [m]')
plt.xlabel('t [s]')
plt.title('Variable Velocity Kalman Filter')
#~ pplt.show()

# plot velocity
#~ vplt = plt.figure(2)
#~ plt.plot(t,vel)
#~ vplt.show()

# plot NIS
#~ nplt = plt.figure(3)
#~ plt.plot(t,NIS)

# plot innovation
#~ iplt = plt.figure(4)
#~ plt.plot(t,inn)

# plot gains
#~ gplt = plt.figure(5)
#~ plt.plot(t,Gains)
#~ nplt.show()

plt.show()

#~ ps = []
#~ for p in cov:
	#~ ps.append(p[(0,0)[0],(0,0)[1]])
#~ plt.plot(ps)
#~ plt.title('Changing Velocity Kalman Filter - Covariance')
#~ plt.show()
