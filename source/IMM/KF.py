#!/usr/bin/env python

# IMM estimator for moving speaker
# Moritz Kampelmuehler
# 

import numpy as np
import matplotlib.pyplot as plt
from filterpy.kalman import KalmanFilter, IMMEstimator
import copy

def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))

# load data
npzfile = np.load('data.npz')
x_true = npzfile['x']
zs = npzfile['z']
T = npzfile['T']
t = npzfile['t']
A = npzfile['A']
sigma_n = npzfile['sigma_n']
sigma_x = npzfile['sigma_x']
n = zs.shape[0]

# IMM params
mu = np.array([0.50, 0.50], dtype=float)
M = np.array([[.98, .02], [.05, .95]], dtype=float)

dim_x = 2
dim_z = 1

# "one point init"
v0 = 0
p0 = zs[0]

# setup filters
KF_wna = KalmanFilter(dim_x, dim_z)
KF_wna.F = A # state transition matrix
KF_wna.x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
KF_wna.P *= (sigma_n)**2 # covariance matrix
KF_wna.R *= (sigma_n)**2 # measurement noise
KF_wna.Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*.71)**2) # process noise
KF_wna.H = np.array([[1,0]], dtype=float) # measurement function

# CV filter
KF_cv = copy.deepcopy(KF_wna)
KF_cv.Q *= 0
KF_cv.F = np.array([[1,T], [0,1]], dtype=float)

bank = IMMEstimator([KF_wna, KF_cv], mu, M)

pos = [None] * n
vel = [None] * n
cov = [None] * n
inncov = [None] * n
inn = [None] * n
NIS = [None] * n
Gains = [None] * n

xs, probs = [], []
wnaxs, cvxs = [], []
xs.append(np.array([[p0], [v0]], dtype=float))
# Perform filtering
for i, z in enumerate(zs):
    z = np.array([z], dtype=float).T
    bank.update(z)

    xs.append(bank.x.copy())
    wnaxs.append(KF_wna.x.copy())
    cvxs.append(KF_cv.x.copy())
    probs.append(bank.mu.copy())
    
for k,x in enumerate(xs):
	if k < n:
		pos[k] = x[0]
		
pos = np.reshape(pos,(n,))

#~ pplt = plt.figure(0)
#~ plt.plot(probs)

mse = ((x_true - pos)**2).mean()
mse_z = ((x_true - zs)**2).mean()

print('estimation MSE: ' + str(mse))
print('input MSE: ' + str(mse_z))
print('input MSE/estimation MSE: ' + str(mse_z/mse))

# plot position, true, estimate + measurement
pplt = plt.figure(1)
p0, = plt.plot(t, x_true)
p1, = plt.plot(t, zs, 'r+')
p2, = plt.plot(t, pos, 'go')
plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=1)
plt.ylim((max(np.min(xs),np.min(zs))-0.5,max(np.max(xs),np.max(zs))+0.5))
plt.ylabel('x [m]')
plt.xlabel('t [s]')
plt.title('IMM Estimator')
#~ pplt.show()

# plot velocity
#~ vplt = plt.figure(2)
#~ plt.plot(t,vel)
#~ vplt.show()

# plot NIS
#~ nplt = plt.figure(3)
#~ plt.plot(t,NIS)

# plot innovation
#~ iplt = plt.figure(4)
#~ plt.plot(t,inn)

# plot gains
#~ gplt = plt.figure(5)
#~ plt.plot(t,Gains)
#~ nplt.show()

plt.show()

#~ ps = []
#~ for p in cov:
	#~ ps.append(p[(0,0)[0],(0,0)[1]])
#~ plt.plot(ps)
#~ plt.title('Changing Velocity Kalman Filter - Covariance')
#~ plt.show()
