#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

# used to generate a data vector for estimation
# vector contains noisy TDOA values, out of which the position of 
# speaker along 1 axis should be estimated
# geometry: microphones assumed to be dM, speaker(s) y0 apart


# constants
dM = float(0.3) #[m]
y0 = float(0.5) #[m]
c = float(343) #[m/s]
T = float(1e0) #[s]
n = 100 # signal length [samples]
A = np.array([[1,T], [0,1]], dtype=float) # transition matrix
B = np.array([[T**2/2], [T]], dtype=float) # control matrix
a = float(0) # constant acceleration
v0 = float(1) # constant velocity

# noise parameters
mu_n = 0
sigma_n = float(1e-1)
mu_x = 0
sigma_x = float(1e-3)

# trajectory boundaries
x0 = -2.5
x1 = 2.5

t = np.arange(5*n) * T

x = np.linspace(x0,x1,5*n)
		
z = x + np.random.normal(mu_n, sigma_n, 5*n)


# store data
np.savez('data.npz', x=x, z=z, T=T, A=A, B=B, a=a, sigma_n=sigma_n, sigma_x=sigma_x, t=t, v0=v0)


# plot data
plt.plot(np.arange(x.shape[0]), x, 'ro')
plt.plot(np.arange(z.shape[0]), z, 'bo')
plt.show()
