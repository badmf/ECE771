#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)
import numpy as np
import matplotlib.pyplot as plt
from filterpy.kalman import KalmanFilter, IMMEstimator
import copy
import os

def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))
	
n_runs = 100
mse_zs = []
mses = []
mseratios = []

for i in range(n_runs):
	
	os.system("./generate_data.py")
	
	# load data
	npzfile = np.load('data.npz')
	x_true = npzfile['x']
	zs = npzfile['z']
	T = npzfile['T']
	t = npzfile['t']
	A = npzfile['A']
	sigma_n = npzfile['sigma_n']
	sigma_x = npzfile['sigma_x']
	n = zs.shape[0]
	
	# IMM params
	mu = np.array([0.5,0.3,0.2], dtype=float)
	M = np.array([[.98, .005, .015], [.005, .98, .015], [.003, .0017, .98]], dtype=float)
	
	dim_x = 2
	dim_z = 1
	
	# "one point init"
	v0 = 0
	p0 = zs[0]
	
	# setup filters
	KF_wna = KalmanFilter(dim_x, dim_z)
	KF_wna.F = A # state transition matrix
	KF_wna.x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
	KF_wna.P *= (sigma_n)**2 # covariance matrix
	KF_wna.R *= ((sigma_n)**2) # measurement noise
	KF_wna.Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*.87)**2) # process noise
	KF_wna.H = np.array([[1,0]], dtype=float) # measurement function
	
	# wna without noise
	KF_nn = copy.deepcopy(KF_wna)
	KF_nn.Q *= 0
	
	# zero velocity no noise filter
	KF_cv = copy.deepcopy(KF_wna)
	KF_cv.Q *= 0
	KF_cv.F = np.array([[1,0], [0,0]], dtype=float)
	
	bank = IMMEstimator([KF_nn, KF_cv, KF_wna], mu, M)
	
	pos = [None] * n
	vel = [None] * n
	
	xs, probs = [], []
	wnaxs, cvxs = [], []
	xs.append(np.array([[p0], [v0]], dtype=float))
	# Perform filtering
	for i, z in enumerate(zs):
	    z = np.array([z], dtype=float).T
	    bank.update(z)
	
	    xs.append(bank.x.copy())
	    wnaxs.append(KF_wna.x.copy())
	    cvxs.append(KF_cv.x.copy())
	    probs.append(bank.mu.copy())
	    
	for k,x in enumerate(xs):
		if k < n:
			pos[k] = x[0]
			vel[k] = x[1]
	pos = np.reshape(pos,(n,))
	vel = np.reshape(vel,(n,))
	
	mse = (((x_true - pos)**2).mean())
	mse_z = (((x_true - zs)**2).mean())
	mses.append(((x_true - pos)**2).mean())
	mse_zs.append(((x_true - zs)**2).mean())
	mseratios.append(mse_z/mse)
	

temp = np.asarray(mseratios)
print(temp.mean())
