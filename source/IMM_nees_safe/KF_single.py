#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (WNA)
# Moritz Kampelmuehler
# 12/16

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

# constants
T = float(1e0) #[s]

# noise parameters
mu_n = 0
sigma_n = float(1e-1)
mu_x = 0
sigma_x = float(1e-3)

# trajectory boundaries
x0 = -0.5
x1 = 0.5

# long/short trajectory
long_trajectory = False
# plot trajectory on every run
plot_trajectory = False
# number of simulation runs
n_runs = 100


def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))
	
def generateData():
	# create trajectory
	n = 50 # period length [samples]
	pos = np.vstack((np.reshape(np.linspace(x0,x1,n), (n,1)), np.ones((n,1)) * x1, np.reshape(np.linspace(x1,x0,n), (n,1)), np.ones((2*n,1)) * x0))
	vel = np.vstack((np.ones((n,1)) * (x1-x0)/n/T, np.zeros((n,1)), np.ones((n,1)) * (x1-x0)/n/T * (-1), np.zeros((2*n,1))))
	
	# repeat for 'long' trajectory
	if long_trajectory:
		pos = np.tile(pos,(5,1))
		vel = np.tile(vel,(5,1))
		
	true_state = np.hstack((pos, vel))		
		
	t = np.arange(np.shape(pos)[0]) * T
	z = pos + np.random.normal(mu_n, sigma_n, np.shape(pos))
	
	return z, t, pos, true_state

MSE_factor = [None] * n_runs
NEES_runs = []

for i in range(n_runs):
	
	# generate trajectory
	zs, t, x_true, states = generateData()
	
	# data length
	n_data = zs.shape[0]
	
	dim_x = 2
	dim_z = 1
	
	# SP init
	v0 = 0
	p0 = zs[0,0]
	vmax = 5/3.6
	
	
	# define constant parameters
	x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
	F = np.array([[1,T], [0,1]], dtype=float) # state transition matrix
	H = np.array([[1,0]], dtype=float) # Measurement function
	R = np.eye(dim_z) * ((sigma_n)**2) # measurement noise
	#~ Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*.71)**2) # process noise
	Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*2.81)**2) # process noise
	P = np.array([[(sigma_n)**2, 0], [0, vmax**2/3]], dtype=float) # covariance matrix
	
	pos = [None] * n_data
	vel = [None] * n_data
	cov = [None] * n_data
	inncov = [None] * n_data
	inn = [None] * n_data
	NIS = [None] * n_data
	NEES = np.zeros((n_data,), dtype=float)
	Gains = [None] * n_data
	
	# Kalman Filter
	for k in range(n_data):
		pos[k] = x[0,0]
		vel[k] = x[1,0]
		cov[k] = P
			
		# predict
		x_est = np.dot(F,x)
		P_est = dot3(F,P,F.T) + Q
		
		# update
		v = zs[(k,0)] - np.dot(H,x_est)
		S = dot3(H,P_est,H.T) + R
		
		W = dot3(P_est,H.T,np.linalg.inv(S))
		x = x_est + np.dot(W,v)
		# Joseph form covariance update
		I_WH = np.eye(dim_x) - np.dot(W,H)
		P = dot3(I_WH,P_est,I_WH.T)+dot3(W,R,W.T)
	
		inncov[k] = S[0]
		inn[k] = v[0]
		NIS[k] = dot3(v.T,np.linalg.inv(S),v)[0]
		x_tilda = np.reshape(states[k,:], np.shape(x)) - x
		NEES[k,] = dot3(x_tilda.T,np.linalg.inv(P),x_tilda)
		Gains[k] = W[0]
	
	mse = ((x_true[:,0] - pos)**2).mean()
	mse_z = ((x_true[:,0] - zs[:,0])**2).mean()
	MSE_factor[i] = mse_z/mse
	NEES_runs.append(NEES)

	# plot position, true, estimate + measurement
	if plot_trajectory:
		pplt = plt.figure(1)
		p0, = plt.plot(t, x_true)
		p1, = plt.plot(t,zs[:,0], linestyle='dashed')
		p2, = plt.plot(t,pos)
		plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=1)
		plt.ylim((max(np.min(pos),np.min(zs))-0.5,max(np.max(pos),np.max(zs))+0.5))
		plt.ylabel('x [m]')
		plt.xlabel('t [s]')
		plt.title('Variable Velocity Kalman Filter')
		pplt.show()
	
# chi square test
NEES_mean = np.mean(NEES_runs, axis=0)
chi2_bounds = scipy.stats.chi2.interval(0.95,n_runs*dim_x)
outliers = ((chi2_bounds[0]/n_runs > NEES_mean) | (NEES_mean > chi2_bounds[1]/n_runs)).sum()
print('chi2 outliers: ' + str(outliers) + ' of ' + str(n_data))
print('chi2 lower: ' + str(chi2_bounds[0]/n_runs) + ' upper: ' + str(chi2_bounds[1]/n_runs))

print('average MSE factor: ' + str(np.asarray(MSE_factor).mean()))
