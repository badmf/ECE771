#!/usr/bin/env python

# IMM estimator for moving speaker
# Moritz Kampelmuehler
# 12/16

import numpy as np
import matplotlib.pyplot as plt
from filterpy.kalman import KalmanFilter, IMMEstimator
import copy
import scipy.stats

# constants
T = float(1e0) #[s]

# noise parameters
mu_n = 0
sigma_n = float(1e-1)
mu_x = 0
sigma_x = float(1e-3)

# trajectory boundaries
x0 = -0.5
x1 = 0.5

# long/short trajectory
long_trajectory = True
# plot trajectory on every run
plot_trajectory = True
# number of simulation runs
n_runs = 1


def dot3(A,B,C):
	# matrix multiplication A*B*C
	return np.dot(A, np.dot(B,C))
	
def generateData():
	n = 100 # period length [samples]
	# create trajectory
	pos = np.vstack((np.reshape(np.linspace(x0,x1,n), (n,1)), np.ones((n,1)) * x1, np.reshape(np.linspace(x1,x0,n), (n,1)), np.ones((2*n,1)) * x0))
	vel = np.vstack((np.ones((n,1)) * (x1-x0)/n/T, np.zeros((n,1)), np.ones((n,1)) * (x1-x0)/n/T * (-1), np.zeros((2*n,1))))
	
	# repeat for 'long' trajectory
	if long_trajectory:
		pos = np.tile(pos,(5,1))
		vel = np.tile(vel,(5,1))
	
	true_state = np.hstack((pos, vel))
		
	t = np.arange(np.shape(pos)[0]) * T
	z = pos + np.random.normal(mu_n, sigma_n, np.shape(pos))
	
	return z, t, pos, true_state

MSE_factor = [None] * n_runs
NEES_runs = []

for i in range(n_runs):
	
	# generate trajectory
	zs, t, x_true, states = generateData()
	
	# data length
	n_data = zs.shape[0]
	
	# IMM params
	mu = np.array([0.5,0.3,0.2], dtype=float)
	M = np.array([[.92, .02, .06], [.025, .95, .025], [.025, .025, .95]], dtype=float)
	
	dim_x = 2
	dim_z = 1
	
	# SP init
	v0 = 0
	p0 = zs[0,0]
	vmax = 5/3.6
		
	# setup filters
	KF_wna = KalmanFilter(dim_x, dim_z)
	KF_wna.F = np.array([[1,T], [0,1]], dtype=float) # state transition matrix
	KF_wna.x = np.array([[p0], [v0]], dtype=float) # initial state (location and velocity)
	KF_wna.P = np.array([[(sigma_n)**2, 0], [0, vmax**2/3]], dtype=float) # covariance matrix
	KF_wna.R *= ((sigma_n)**2) # measurement noise
	KF_wna.Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]], dtype=float)*((sigma_x*3.4)**2) # process noise
	KF_wna.H = np.array([[1,0]], dtype=float) # measurement function
	
	# wna without noise
	KF_nn = copy.deepcopy(KF_wna)
	KF_nn.Q *= 0
	
	# zero velocity no noise filter
	KF_cv = copy.deepcopy(KF_wna)
	KF_cv.Q *= 0
	KF_cv.F = np.array([[1,0], [0,0]], dtype=float)
	
	bank = IMMEstimator([KF_nn, KF_cv, KF_wna], mu, M)
	
	pos = [None] * n_data
	NEES = np.zeros((n_data,), dtype=float)
	
	xs, probs = [], []
	xs.append(np.array([[p0], [v0]], dtype=float))
	# Perform filtering
	for k,z in enumerate(zs):
	    z = np.array([z], dtype=float).T
	    bank.update(z)

	    xs.append(bank.x.copy())
	    probs.append(bank.mu.copy())
	    x_tilda = np.reshape(states[k,:], np.shape(bank.x)) - bank.x
	    NEES[k,] = dot3(x_tilda.T,np.linalg.inv(bank.P),x_tilda)

	for k,x in enumerate(xs):
		if k < n_data:
			pos[k] = x[0]

	mse = ((x_true[:,0] - np.asarray(pos)[:,0])**2).mean()
	mse_z = ((x_true[:,0] - zs[:,0])**2).mean()
	MSE_factor[i] = mse_z/mse
	NEES_runs.append(NEES)

	# plot position, true, estimate + measurement
	if plot_trajectory:
		plt.figure(0)
		p0, = plt.plot(t, x_true)
		p1, = plt.plot(t,zs[:,0], linestyle='dashed')
		p2, = plt.plot(t,pos)
		plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=1)
		plt.ylim((max(np.min(pos),np.min(zs))-0.5,max(np.max(pos),np.max(zs))+0.5))
		plt.ylabel('x [m]')
		plt.xlabel('t [s]')
		plt.title('IMM Estimator')
		plt.figure(1)
		plt.plot(probs)
		plt.legend(['nn', 'cv', 'wna'])
		plt.show()

# chi square test
NEES_mean = np.mean(NEES_runs, axis=0)
chi2_bounds = scipy.stats.chi2.interval(0.9,n_runs*dim_x)
outliers = ((chi2_bounds[0]/n_runs > NEES_mean) | (NEES_mean > chi2_bounds[1]/n_runs)).sum()
print('chi2 outliers: ' + str(outliers) + ' of ' + str(n_data))
print('chi2 lower: ' + str(chi2_bounds[0]/n_runs) + ' upper: ' + str(chi2_bounds[1]/n_runs))

# MSE factor
print('average MSE factor: ' + str(np.asarray(MSE_factor).mean()))

