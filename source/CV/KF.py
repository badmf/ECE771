#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)

from filterpy.kalman import KalmanFilter
import numpy as np
import matplotlib.pyplot as plt
import pylab as pl

def Q_DWPA(dim, dt=1., sigma=1.):
	""" Returns the Q matrix for the Discrete Wiener Process Acceleration Model.
	dim may be either 2 or 3, dt is the time step, and sigma is the variance in
	the noise"""
	assert dim == 2 or dim == 3
	if dim == 2:
		Q = np.array([[.25*dt**4, .5*dt**3],
					[ .5*dt**3,    dt**2]], dtype=float)
	else:
		Q = np.array([[.25*dt**4, .5*dt**3, .5*dt**2],
					[ .5*dt**3,    dt**2,       dt],
					[ .5*dt**2,       dt,        1]], dtype=float)
	return Q * sigma

# load data
npzfile = np.load('data.npz')
x = npzfile['x']
zs = npzfile['z']
T = npzfile['T']
A = npzfile['A']
sigma_n = npzfile['sigma_n']
sigma_x = npzfile['sigma_x']

# data length
n = zs.shape[0]

# initialize filter
kf = KalmanFilter (dim_x=2, dim_z=1)

# define constant parameters
kf.x = np.array([[0], [0]]) # initial state (location and velocity)
kf.F = A # state transition matrix
kf.H = np.array([[1,0]]) # Measurement function
kf.R *= sigma_n # measurement noise
kf.Q = Q_DWPA(2, T, sigma=sigma_x) # process noise
kf.P *= 1e3 # covariance matrix

# execute filter
pos = [None] * n
cov = [None] * n
for t in range(n):
	z = zs[t]
	pos[t] = kf.x[0,0]
	cov[t] = kf.P
	# perform the kalman filter steps
	kf.update (z)
	kf.predict()

p0, = plt.plot(range(1,n+1), x)
p1, = plt.plot(range(1,n+1),zs, linestyle='dashed')
p2, = plt.plot(range(1,n+1),pos)
plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=2)
plt.ylim((np.min(zs)-0.5,np.max(zs)+0.5))
plt.title('Constant Velocity Kalman Filter')
plt.show()

#~ ps = []
#~ for p in cov:
	#~ ps.append(p[(0,0)[0],(0,0)[1]])
#~ plt.plot(ps)
#~ plt.title('Constant Velocity Kalman Filter - Covariance')
#~ plt.show()
