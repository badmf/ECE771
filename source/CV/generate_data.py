#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

# used to generate a data vector for estimation
# vector contains noisy TDOA values, out of which the position of 
# speaker along 1 axis should be estimated
# geometry: microphones assumed to be dM, speaker(s) y0 apart


# constants
dM = 0.3 #[m]
y0 = 0.5 #[m]
c = 343 #[m/s]
T = 1e-1 #[s]
n = 100 # signal length [samples]
A = np.array([[1,T], [0,1]]) # transition matrix
B = np.array([[T**2/2], [T]]) # control matrix
a = 0 # constant acceleration
v0 = 1 # constant velocity

# noise parameters
mu_n = 0
sigma_n = 5e-1
mu_x = 0
sigma_x = 1e-2

# trajectory boundaries
x0 = -2
x1 = 1

t = np.arange(n) * T

x = np.ones(n)*x0 + v0*t + a/2*t**2 + np.random.normal(mu_x, sigma_x, n)

z = x + np.random.normal(mu_n, sigma_n, n)


# store data
np.savez('data.npz', x=x, z=z, T=T, A=A, B=B, a=a, sigma_n=sigma_n, sigma_x=sigma_x, t=t, v0=v0)


# plot data
plt.plot(np.arange(x.shape[0]), x, 'ro')
plt.plot(np.arange(z.shape[0]), z, 'bo')
plt.show()
