#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)

from pykalman import KalmanFilter
import numpy as np
import matplotlib.pyplot as plt
import pylab as pl

# load data
npzfile = np.load('data.npz')
x = npzfile['x']
z = npzfile['z']

# data length
n = z.shape[0]

# define constant parameters
C = np.array([1, 0])
b = np.array([0, 0]) 
d = np.array([0, 0])
Q0 = np.array([[5e-1, 0], [0, 5e-1]])
R0 = np.array([[5e-1, 0], [0, 5e-1]])
A = np.array([[1, 1], [0, 1]])
x0 = np.array([1, 1])
P0 = np.array([[1e3, 0],[0, 1e3]])

kf = KalmanFilter(
    A,  #transition_matrix
    C,  #observation_matrix
    Q0, #initial_transition_covariance
    R0, #initial_observation_covariance
    b,  #transition_offsets
    d,  #observation_offset
    x0, #initial_state_mean
    P0  #initial_state_covariance
)

n_timesteps = n
n_dim_state = 2
filtered_state_means = np.zeros((n_timesteps, n_dim_state))
filtered_state_covariances = np.zeros((n_timesteps, n_dim_state, n_dim_state))
for t in range(n_timesteps - 1):
    if t == 0:
        filtered_state_means[t] = x0
        filtered_state_covariances[t] = P0
    filtered_state_means[t + 1], filtered_state_covariances[t + 1] = (
        kf.filter_update(
            filtered_state_means[t],
            filtered_state_covariances[t],
            z[t + 1]
        )
    )
    
    
# draw estimates
pl.figure()
lines_true = pl.plot(z, color='b')
lines_filt = pl.plot(filtered_state_means, color='r')
pl.legend((lines_true[0], lines_filt[0]), ('true', 'filtered'))
pl.show()
