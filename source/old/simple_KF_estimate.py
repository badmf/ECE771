#!/usr/bin/env python

# KF (vs. LPF)

import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

def KF(x, z, u, P, A, B, H, Q, R):
	x_hat = A*x + B*u
	P_hat = A * P * A + Q
	inn = z - H * x_hat
	S = H * P_hat * H + R
	K = P_hat * H / S
	x = x_hat + K * inn
	P = (1 - K * H) * P_hat
	return x, P

# load data
npzfile = np.load('data_nonl_standstill.npz')
x = npzfile['x']
z = npzfile['z']

# data length
n = x.shape[0]

u = 1
P = 1e6
A = 1
B = 0
H = 1
Q = 1e-9
R = 1
x_hat = [0]
P_hat = [0]

x_hat[0], P_hat[0] = KF(z[0], z[0], u, P, A, B, H, Q, R)

# execute estimation
for i, zi in enumerate(z[1:]):
	x_pred, P = KF(x_hat[i], zi, u, P_hat[i], A, B, H, Q, R)
	x_hat.append(x_pred)
	P_hat.append(P)
	

	
# LP filtering
h=signal.firwin(numtaps=100, cutoff=.1, nyq=24000)
x_LP=signal.lfilter( h, 1.0, z)

# plot results
plt.plot(np.arange(n), x, 'ro')
plt.plot(np.arange(n), x_hat, 'bo')
plt.plot(np.arange(n), z, 'go')
plt.plot(np.arange(n), x_LP, 'bx')
plt.show()

# plot covariance evolution
#~ plt.plot(np.arange(n), P_hat, 'bx')
#~ plt.show()
