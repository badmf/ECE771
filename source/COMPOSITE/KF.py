#!/usr/bin/env python

# Kalman Filter implementation for moving speaker (Constant Velocity)

from filterpy.kalman import KalmanFilter
import numpy as np
import matplotlib.pyplot as plt
import pylab as pl

# load data

npzfile = np.load('data.npz')
x = npzfile['x']
zs = npzfile['z']
T = npzfile['T']
t = npzfile['t']
A = npzfile['A']
sigma_n = npzfile['sigma_n']
sigma_x = npzfile['sigma_x']
a = npzfile['a']
B = npzfile['B']

# data length
n = zs.shape[0]

# initialize filter
kf = KalmanFilter (dim_x=2, dim_z=1, dim_u=1)

# define constant parameters
kf.x = np.array([[0], [0]]) # initial state (location and velocity)
kf.u = np.array([a]) # acceleration
kf.F = A # state transition matrix
kf.H = np.array([[1,0]]) # Measurement function
kf.B = B  # control function
#~ kf.B = B * 0 # control function
kf.R *= sigma_n**2 # measurement noise
kf.Q = np.array([[.25*T**4, .5*T**3], [.5*T**3, T**2]])*((sigma_x)**2) # process noise
#~ kf.Q = np.array([[1, 0], [0, 1]])*(sigma_x)**2 # process noise
kf.P *= 1e3 # covariance matrix

# execute filter
pos = [None] * n
cov = [None] * n
for i in range(n):
	z = zs[i]
	pos[i] = kf.x[0,0]
	cov[i] = kf.P
	# perform the kalman filter steps
	kf.update (z)
	kf.predict(a)

	# 'cheat'
	#~ if i > n/3:
		#~ a = 0
	#~ if i == n/3:
		#~ kf.P *= 1e4
	#~ if i == 2*n/3:
		#~ kf.P *= 1e4
	#~ if i > 2*n / 3:
		#~ kf.x = np.array([[kf.x[0,0]], [0]])

print('mse: ' + str(((x - pos)**2).mean()))

p0, = plt.plot(t, x)
p1, = plt.plot(t,zs, linestyle='dashed')
p2, = plt.plot(t,pos)
plt.legend([p0,p1,p2], ['actual','measurement', 'filter'], loc=2)
plt.ylim((np.min(zs)-0.5,np.max(zs)+0.5))
plt.title('Changing Velocity Kalman Filter')
plt.show()

#~ ps = []
#~ for p in cov:
	#~ ps.append(p[(0,0)[0],(0,0)[1]])
#~ plt.plot(ps)
#~ plt.title('Changing Velocity Kalman Filter - Covariance')
#~ plt.show()
